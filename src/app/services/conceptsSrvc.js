'use strict';
 
angular.module('codr').factory('conceptsSrvc', ['$http', '$q', function($http, $q){
 
    var REST_SERVICE_URI = 'http://codrapp.tk:5000/';

    var factory = {
        addUser: addUser,
        addInterest: addInterest,
        getUser:getUser,
        getInterests:getInterests,
        removeInterest:removeInterest,
        getChallenges:getChallenges,
        getChallenge:getChallenge,
        getSolutions:getSolutions,
        getSolution:getSolution,
        queryDBPediaAlgorithms:queryDBPediaAlgorithms,
        getCodeForces:getCodeForces,
        filterChallenges:filterChallenges

    };
 
    return factory;
 
    function getUser(userid) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'getUser?username='+userid)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function filterChallenges(participants,difficulty,IRI,tags) {
    	console.log(tags);
    	var from_tags='';
    	
    	if(typeof tags != "undefined"){
    	for(var i=0;i<tags.length;i++){
    		if(tags.length>1){
    		from_tags=from_tags+tags[i].text+',';
    		}
    		else{
    			from_tags=tags[0].text;
    		}

			
    	}
    	if(tags.length>1){
    		from_tags = from_tags.slice(0, -1);
    	}
    }
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'filterChallenges?participants='+participants+'&difficulty='+difficulty+'&IRI='+IRI+'&tags='+from_tags)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function addUser(user) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI+'addUser', user)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
     function addInterest(interest) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI+'addInterest', interest)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating interest');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

        function getInterests(userid) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'getInterests?username='+userid)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching interests for user');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function removeInterest(username,tag) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI+'removeInterest?username='+username+'&tag='+tag)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting interest for user');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

            function getChallenges(category,number) {
                var path;
                if(typeof number != 'undefined'){
                    path=REST_SERVICE_URI+'getChallenges?domain='+category+'&number='+number;
                }
                else{
                    path=REST_SERVICE_URI+'getChallenges?domain='+category;
                }
        var deferred = $q.defer();
        $http.get(path)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching challenges for category');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

            function getChallenge(contest) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'getChallenge?contest='+contest)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching challenge');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
                function getSolutions(contest) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'getSolutions?contest='+contest)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching solutions');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
           function getSolution(contest) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'getSolution?solution='+contest)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching solutions');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

        function queryDBPediaAlgorithms(tags) {
        var all_tags;
        for(tag in tags){
            all_tags=all_tags+','+x;
        }
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+'queryDBPediaAlgorithms?tags='+all_tags)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching recommendations');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

        function getCodeForces() {
        var deferred = $q.defer();
        $http.get("http://codeforces.com/api/contest.list/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching codeforces');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);