angular.module('codr').controller('filterController', ['$scope','conceptsSrvc','$location', '$rootScope',function ($scope,conceptsSrvc,$location,$rootScope) {
	$scope.filters={};
	$scope.programmingLanguages=["Java","C#","HTML","C++"];
	$scope.problemSources=[{name:"FreeCodeCamp",uri:'http://www.freecodecamp.com'}];
	$scope.allTags=['java','javascript','shortest-path','divide et impera','xml'];
	$scope.filter=function(){
		console.log($scope.filters);
		retrieveAllData();
		
	}

	 $scope.loadTags = function() {
     return $scope.allTags;
  };

  

	function retrieveAllData(){
		var difficulty;
		if($scope.filters.difficultyEasy==true){
			difficulty="easy";
		}
		if($scope.filters.difficultyMedium==true){
			difficulty="medium";
		}
		if($scope.filters.difficultyHard==true){
			difficulty="hard";
		}

        conceptsSrvc.filterChallenges($scope.filters.participants,difficulty,$scope.filters.problemSource,$scope.filters.tags)
            .then(
            function(d) {
               $rootScope.challenges= d;
               console.log($rootScope.challenges);
               $location.path('/search');
            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
     }

}])