angular.module('codr').controller('contestController', ['$scope','NgTableParams','conceptsSrvc'
, function ($scope,NgTableParams,conceptsSrvc
) {
	
newContests=[];
oldContests=[];
	fetchAllCodeForces();
    function fetchAllCodeForces(){
        conceptsSrvc.getCodeForces()
            .then(
            function(d) {
                $scope.contests = d.result;
                 for(x in $scope.contests){
                 
                 	$scope.contests[x].duration=$scope.contests[x].durationSeconds/60;
                 	$scope.contests[x].start_date=toDateTime($scope.contests[x].startTimeSeconds);
                 	if($scope.contests[x].phase=="FINISHED"){
                 		oldContests.push($scope.contests[x]);
                 	}
                 	else{
                 		newContests.push($scope.contests[x]);

                 	}
                 }
                 $scope.newContestsTable = new NgTableParams({}, { dataset: newContests});
				 $scope.oldContestsTable = new NgTableParams({}, { dataset: oldContests});
            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
    }

    function toDateTime(secs) {
    var t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
}


}])
