angular.module('codr').controller('solutionController', ['$scope', '$route', '$routeParams','conceptsSrvc'
	,function ($scope,$route,$routeParams,conceptsSrvc) {
	console.log($routeParams.solutionId);
	$scope.solutionId=$routeParams.solutionId;
	$scope.solution={}
    
	retrieveAllData();

	function retrieveAllData(){
        conceptsSrvc.getSolution($scope.solutionId)
            .then(
            function(d) {
               $scope.solution= d;
               console.log($scope.solution);
            },
            function(errResponse){
                console.error('Error while fetching solution');
            }
        );
     }
}])