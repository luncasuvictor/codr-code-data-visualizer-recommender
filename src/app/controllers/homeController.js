angular.module('codr').controller('homeController', ['$scope','conceptsSrvc', function ($scope,conceptsSrvc) {
	$scope.categories=['Frontend','Scripting','Databases','Others'];
	$scope.challengesFrontend=[];
	$scope.challengesScripting=[];
	$scope.challengesDatabases=[];
	$scope.challengesOthers=[];
retrieveAllData();
	function retrieveAllData(){
        conceptsSrvc.getChallenges('Frontend',3)
            .then(
            function(d) {
               $scope.challengesFrontend= d;
               console.log('1 returned');

            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
                    conceptsSrvc.getChallenges('Scripting',3)
            .then(
            function(d) {
               $scope.challengesScripting= d;
                console.log('2 returned');

            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
    
                        conceptsSrvc.getChallenges('Databases',3)
            .then(
            function(d) {
               $scope.challengesDatabases= d;
                console.log('3 returned');

            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
                                conceptsSrvc.getChallenges('Others',3)
            .then(
            function(d) {
               $scope.challengesOthers= d;
                console.log('4 returned');

            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
	
	}
	 
}]);