angular.module('codr').controller('categoryController', ['$scope', '$route', '$routeParams','conceptsSrvc',function ($scope,$route,$routeParams,conceptsSrvc) {
	console.log($routeParams.categoryId);
	$scope.categoryId=$routeParams.categoryId;
	$scope.challenges=[];

	retrieveAllData();
	function retrieveAllData(){
        conceptsSrvc.getChallenges($scope.categoryId)
            .then(
            function(d) {
               $scope.challenges= d;
             
            },
            function(errResponse){
                console.error('Error while fetching contests');
            }
        );
     }
	$scope.tags=['java','.net'];

	$scope.topChallenges=[{name:'Say Hello To HTML',challengeId:'1'},{name:'First responsive app',challengeId:'1'},{name:'CSS for begineers',challengeId:'1'}];
}])