var scotchApp = angular.module('codr');

    // configure our routes
    scotchApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'views/login.html',
                controller  : 'loginController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'views/about-contact.html',
                controller  : 'aboutController'
            })
            .when('/home', {
                templateUrl : 'views/home.html',
                controller  : 'homeController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'views/about-contact.html',
                controller  : 'aboutController'
            })
            .when('/futurecontests', {
                templateUrl : 'views/future-contests.html',
                controller  : 'contestController'
            })
            .when('/pastcontests', {
                templateUrl : 'views/past-contests.html',
                controller  : 'contestController'
            })
            .when('/login', {
                templateUrl : 'views/login.html',
                controller  : 'loginController'
            })
            .when('/category/:categoryId', {
                templateUrl : 'views/category.html',
                controller  : 'categoryController'
            })
            .when('/challenge/:challengeId', {
                templateUrl : 'views/challenge.html',
                controller  : 'challengeController'
                ,
                resolve:{
                    "challenge": function ($q, $http,$route) {
                             var deferred = $q.defer();
                                $http.get('http://codrapp.tk:5000/getChallenge?contest='+$route.current.params.challengeId)
                            .then(
                            function (response) {
                                console.log('in resolve');
                                console.log(response.data);
                                deferred.resolve(response.data);
                            },
                            function(errResponse){
                                console.error('Error while fetching challenge');
                                deferred.reject(errResponse);
                            }
                            );
                            return deferred.promise;
                     },
                    "solutions": function ($q, $http,$route) {
                             var deferred = $q.defer();
                            $http.get('http://codrapp.tk:5000/getSolutions?contest='+$route.current.params.challengeId)
                                .then(
                                function (response) {
                                    deferred.resolve(response.data);
                                },
                                function(errResponse){
                                    console.error('Error while fetching solutions');
                                    deferred.reject(errResponse);
                                }
                             );
                            return deferred.promise;
                     } 
                }
            })
            .when('/solution/:solutionId', {
                templateUrl : 'views/solution.html',
                controller  : 'solutionController'
            })
            .when('/settings', {
                templateUrl : 'views/settings.html',
                controller  : 'settingsController'
            })
                       .when('/search', {
                templateUrl : 'views/category.html',
                controller  : 'searchController'
            })
    });
