import json
from flask import request
from SPARQLWrapper import SPARQLWrapper, JSON
from flask import Response, jsonify
from repository import *

from flask import Flask
app = Flask(__name__)

from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@app.route('/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def hello_world():
    return 'Hello, World!'


@app.route('/addTag', methods=["POST"])
@crossdomain(origin='*')
def api_addTag():
    tag_name = request.form["name"]
    tag_desc = request.form["desc"]
    addTag(tag_name, tag_desc)
    return "Success!"

@app.route('/addUser', methods=["POST"])
@crossdomain(origin='*')
def api_addUser():
    user_id = request.form["username"]
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    mail = request.form["email"]
    addUser(user_id, first_name, last_name, mail)
    return "Success!"

@app.route('/getUser', methods=["GET"])
@crossdomain(origin='*')
def api_getUser():
    user_id = request.args.get("username")
    return getUser(user_id)

@app.route('/addInterest', methods=["POST"])
@crossdomain(origin='*')
def api_addInterest():
    user_id = request.form["username"]
    tag_id = request.form["tag"]
    addInterest(user_id, tag_id)
    return "Success!"

@app.route('/getInterests', methods=["GET"])
@crossdomain(origin='*')
def api_getInterests():
    user_id = request.args.get("username")
    data = getInterests(user_id)
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/rdf+xml'
    )
    return response

@app.route('/removeInterest', methods=["DELETE"])
@crossdomain(origin='*')
def api_removeInterest():
    user_id = request.args.get("username")
    tag_id = request.args.get("tag")
    removeInterest(user_id, tag_id)
    return "Success!"

@app.route('/getChallenges', methods=["GET"])
@crossdomain(origin='*')
def api_getChallenges():
    domain_id = request.args.get("domain")
    nr = request.args.get("number")
    data = getChallenges(domain_id, nr)
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/getChallenge', methods=["GET"])
@crossdomain(origin='*')
def api_getChallenge():
    contest_id = request.args.get("contest")
    return getChallenge(contest_id)

@app.route('/getSolutions', methods=["GET"])
@crossdomain(origin='*')
def api_getSolutions():
    contest_id = request.args.get("contest")
    return getSolutions(contest_id)

@app.route('/getSolution', methods=["GET"])
@crossdomain(origin='*')
def api_getSolution():
    solution_id = request.args.get("solution")
    return getSolution(solution_id)

@app.route('/addSolution', methods=["POST"])
@crossdomain(origin='*')
def api_addSolution():
    solution_id = request.form["solution"]
    text = request.form["text"]
    lang = request.form["language"]
    repository = request.form["repository"]
    submission_date = request.form["submission_date"]
    contest_id = request.form["contest"]
    addSolution(solution_id, text, lang, repository, submission_date, contest_id)
    return "Success!"

@app.route('/removeChallenge', methods=["DELETE"])
@crossdomain(origin='*')
def api_removeChallenge():
    contest_id = request.args.get("contest")
    removeChallenge(contest_id)
    return "Success!"

@app.route('/removeSolution', methods=["DELETE"])
@crossdomain(origin='*')
def api_removeSolution():
    solution_id = request.args.get("solution")
    removeSolution(solution_id)
    return "Success!"

@app.route('/addChallenge', methods=["POST"])
@crossdomain(origin='*')
def api_addChallenge():
    contest_id = request.form["contest"]
    name = request.form["name"]
    desc = request.form["desc"]
    url = request.form["url"]
    start_date = request.form["start_date"]
    end_date = request.form["end_date"]
    difficulty = request.form["difficulty"]
    category = request.form["category"]
    domain = request.form["domain"]
    tags = request.form["tags"]
    addChallenge(contest_id, name, desc, url, start_date, end_date, difficulty, category, domain, tags)
    return "Success!"

@app.route('/setParticipants', methods=["POST"])
@crossdomain(origin='*')
def api_setParticipants():
    solution_id = request.form["contest"]
    nr = request.form["participants"]
    setParticipants(solution_id, nr)
    return "Success!"

@app.route('/filterChallenges', methods=["GET"])
@crossdomain(origin='*')
def api_filterChallenges():
    participants = request.args.get("participants")
    difficulty = request.args.get("difficulty")
    IRI = request.args.get("IRI")
    tags = request.args.get("tags")
    return filterChallenges(participants, difficulty, IRI, tags)
    #lang, participants, difficulty, source, tags


@app.route('/getRecommendations', methods=["GET"])
@crossdomain(origin='*')
def api_getRecommendations():
    tags = request.args.get("tags")
    data = getRecommendations(tags)
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response
    #lang, participants, difficulty, source, tags


@app.route("/sparql", methods=['GET'])
@crossdomain(origin='*')
def sparql_endpoint(JSON_LD=True):
    q = request.values["query"]
    data = do_sparql_query(q)
    if JSON_LD:
        response = app.response_class(
            response=json.dumps(data),
            status=200,
            mimetype='application/json+ld'
        )
    else:
        response = app.response_class(
            response=json.dumps(data),
            status=200,
            mimetype='application/rdf+xml'
        )
    return response