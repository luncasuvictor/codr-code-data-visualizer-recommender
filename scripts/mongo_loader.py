import json
import sys
from sparql_query import *
import pymongo
import datetime
from pymongo import MongoClient
from pygments.lexers import guess_lexer
from repository import *
import random

client = MongoClient('localhost', 27017)

db = client.CoDR

solutions = db.solutions

what_contest_id = {}
what_contest_start_date = {}
what_contest_end_date = {}
what_contest_participants = {}
what_contest_categ = {}
what_contest_diff = {}
what_contest_tags = {}
what_categ = {}
what_categ_id = {}

names = ["Adrian",
        "Adriano",
        "Alex",
        "Alexandru",
        "Alin",
        "Andrei",
        "Aurel",
        "Bogdan",
        "Ciprian",
        "Claudiu",
        "Constantin",
        "Cornel",
        "Cosmin",
        "Costin",
        "Cristi",
        "Cristian",
        "Dan",
        "David",
        "Decebal",
        "Denis",
        "Dorin",
        "Dragos",
        "Eugen",
        "Felix",
        "Florin",
        "Gabriel",
        "George",
        "Horatiu",
        "Horia",
        "Iacob",
        "Ilie",
        "Ioan",
        "Ion",
        "Iulian",
        "Iustin",
        "Laur",
        "Liviu",
        "Lucian",
        "Madalin",
        "Marius",
        "Ovidiu",
        "Petre",
        "Radu",
        "Sergiu",
        "Silviu",
        "Stefan",
        "Victor",
        "Vlad"]

families = ["Pop",
            "Popa",
            "Popescu",
            "Ionescu",
            "Nemes",
            "Stan",
            "Dumitrescu",
            "Dima",
            "Ionita",
            "Marin",
            "Tudor",
            "Dobre",
            "Barbu",
            "Nistor",
            "Florea",
            "Ene",
            "Dinu",
            "Georgescu"]

STAR_DOG = False

def get_date(stamp):
    return datetime.datetime.fromtimestamp(stamp / 1000).strftime('%Y-%m-%d')

def get_tags(name):
    tags = []
    if 'Bootstrap' in name:
        tags.append('bootstrap')
    if 'JavaScript' in name:
        tags.append('javascript')
    if 'Checkboxes' in name or 'HTML' in name:
        tags.append('html')
    if 'Nodejs' in name:
        tags.append('nodejs')
    if 'Number' in name or 'Arithmetic' in name:
        tags.append('algorithms')
    return tags


def load_categories():
    print "Loading categories..."
    fh = open("categories.txt","r")
    data = fh.readlines()
    contest = ""
    cid = 0
    for contest in data:
        contest = contest.strip()
        if len(contest) == 0:
            continue
        if contest[0] == '[':
            last_categ = contest[1:-1]
            continue
        what_categ[contest] = last_categ
        if last_categ not in what_categ_id:
            cid += 1
            what_categ_id[last_categ] = "category-" + str(cid)
            if STAR_DOG:
                upload_CATEGORY(what_categ_id[last_categ], last_categ)
    upload_CATEGORY("category-others", "Others")
    print 'Total categories: ', cid


def load_constests():
    print "Loading contests..."
    fh = open("output-mic.json", "r")
    line = fh.readline().strip()
    cid = 0
    uid = 0
    while True:
        line = fh.readline().strip()
        if line[-1] == ",":
            line = line[:-1]
        if line == "]":
            break
        uid += 1
        obj = json.loads(line)
        for x in obj:
            if 'name' in x and 'completedDate' in x and x['name'] != '':
                c_name = x['name']
                stamp = int(x['completedDate'])
                if not c_name in what_contest_id:
                    cid += 1
                    what_contest_id[c_name] = "contest-" + str(cid)
                    what_contest_start_date[c_name] = stamp
                    what_contest_end_date[c_name] = stamp
                    what_contest_participants[c_name] = 1
                    tags = get_tags(c_name)
                    what_contest_tags[c_name] = tags
                    if "Waypoint: " in c_name:
                        s = c_name.split("Waypoint: ")[1]
                    cat_id = None
                    if s in what_categ:
                        cat_id = what_categ_id[what_categ[s]]
                    if cat_id is None:
                        cat_id = "category-others"
                    what_contest_categ[c_name] = cat_id
                    diff = "easy"
                    if (what_contest_participants[c_name] > 30):
                        diff = "medium"
                    if (what_contest_participants[c_name] > 50):
                        diff = "hard"
                    what_contest_diff[c_name] = diff
                else:
                    what_contest_start_date[c_name] = min(what_contest_start_date[c_name], stamp)
                    what_contest_end_date[c_name] = max(what_contest_end_date[c_name], stamp)
                    what_contest_participants[c_name] += 1
        if uid % 1000 == 0:
            print "Progress: [", uid, "]"
    for contest in what_contest_id:
        what_contest_start_date[contest] = get_date(what_contest_start_date[contest])
        what_contest_end_date[contest] = get_date(what_contest_end_date[contest])
        # print contest, what_contest_start_date[contest], what_contest_end_date[contest], what_contest_participants[contest], what_contest_tags[contest]
        if STAR_DOG:
            addChallenge(what_contest_id[contest], contest, "Free Code Camp Challenge", "http://www.freecodecamp.com",
                     what_contest_start_date[contest], what_contest_end_date[contest], what_contest_diff[contest], what_contest_categ[contest],
                     ",".join(what_contest_tags[contest]))
        # setParticipants(what_contest_id[contest], what_contest_participants[contest])
    print 'Total contests: ', cid

def load_users():
    fh = open("output-mic.json","r")
    line = fh.readline().strip()
    uid = 0
    sid = 0
    langs = set()
    while True:
        line = fh.readline().strip()
        if line[-1] == ",":
            line = line[:-1]
        uid += 1
        if line == "]":
            break
        obj = json.loads(line)
        user_id = "code-camper-" + str(uid)
        # Insert User into Database
        if STAR_DOG:
            upload_PERSON(user_id, names[random.randint(0, len(names)-1)], families[random.randint(0, len(families)-1)])
        for x in obj:
            if 'name' in x and 'completedDate' in x and x['name'] != '':
                c_name = x['name']
                # This contest has a category
                con_id = what_contest_id[c_name]
                if 'completedDate' in x and x['completedDate'] != 0:
                    sol = ''
                    if 'solution' in x:
                        sol = x['solution']
                        stamp = x['completedDate']
                        if sol is not None:
                            sid += 1
                            sol_id = "solution-" + str(sid)
                            if 'http' in sol[:4]:
                                # print sol
                                if STAR_DOG:
                                    addSolution(sol_id, "", "", sol, get_date(stamp), con_id)
                                    #solution_id, text, lang, repository, submission_date, contest_id
                            else:
                                lang = guess_lexer(sol).aliases[0]
                                langs.add(lang)
                                if STAR_DOG:
                                    addSolution(sol_id, "", lang, "", get_date(stamp), con_id)
                                # solutions.insert({"id": sol_id, "text": sol})
                            if STAR_DOG:
                                upload_SET_SOLUTION(user_id, sol_id)

        if uid % 10 == 0:
            print uid
        if uid > 600:
            break
    print str(langs)

load_categories()
load_constests()
load_users()