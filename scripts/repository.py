import json
from SPARQLWrapper import SPARQLWrapper, JSON, RDF, RDFXML, N3
from bson.json_util import dumps
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.CoDR
solutions = db.solutions

query = "http://localhost:5820/CoDR/query"
update = "http://localhost:5820/CoDR/update"
prefix = """
    PREFIX ns: <http://rdfs.org/sioc/ns#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX doap: <http://usefulinc.com/ns/doap#>
    PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

"""


def serialize(obj, with_dump=True):
    results = []
    for result in obj["results"]["bindings"]:
        r = {}
        for var in obj["head"]["vars"]:
            r[var] = result[var]["value"]
        results.append(r)
    if with_dump:
        return json.dumps(results, indent=1)
    else:
        return results


def get_sparql(update_flag = False):
    if update_flag:
        sparql = SPARQLWrapper(update)
        sparql.method = 'POST'
    else:
        sparql = SPARQLWrapper(query)
        sparql.setReturnFormat(JSON)
    sparql.setCredentials("admin", "admin")
    return sparql


def addUser(user_id, first_name, last_name, email):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
          [] rdf:type       foaf:Person ;
             ns:id          "{0}" ;
             foaf:firstName "{1}" ;
             foaf:lastName  "{2}" ;
             foaf:mbox      "{3}" .
        }} WHERE {{
           FILTER NOT EXISTS {{
              [] rdf:type       foaf:Person ;
                 ns:id      "{0}" .
          }}
        }}
    """.format(user_id, first_name, last_name, email))
    results = sparql.query()


def addInterest(user_id, tag_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
          ?s codr:hasInterest ?tag .
        }}
        WHERE {{
          ?s rdf:type foaf:Person ;
             ns:id  "{0}" .
          ?tag rdf:type codr:Tag ;
               codr:name "{1}" .
        }}
    """.format(user_id, tag_id))
    results = sparql.query()


def getUser(user_id):
    sparql = get_sparql()
    sparql.setQuery(prefix + """
        SELECT ?firstName ?lastName ?mbox
        WHERE {{
          [] rdf:type       foaf:Person ;
             ns:id          "{0}" ;
             foaf:firstName ?firstName ;
             foaf:lastName  ?lastName ;
             foaf:mbox      ?mbox .
        }}""".format(user_id))
    results = sparql.query().convert()
    return serialize(results)


def getInterests(user_id):
    sparql = get_sparql()
    sparql.setQuery(prefix + """
        SELECT ?tag
        WHERE {{
          ?s rdf:type foaf:Person ;
             ns:id            "{0}" ;
             codr:hasInterest ?p .
          ?p rdf:type codr:Tag ;
             codr:name ?tag .
        }}""".format(user_id))
    #results = sparql.query().convert()
    #return serialize(results)
    results = sparql.query().convert()
    return serialize(results)


def removeInterest(user_id, tag_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        DELETE{{
          ?p codr:hasInterest ?t ;
        }}
        WHERE {{
          ?p rdf:type foaf:Person ;
             ns:id    "{0}" .

          ?t rdf:type codr:Tag ;
             codr:name  "{1}" .
        }}
    """.format(user_id, tag_id))
    results = sparql.query()


def getChallenges(domain, nr = None):
    sparql = get_sparql()
    q = prefix + """
        SELECT ?id ?name ?desc ?start_date ?end_date ?IRI ?difficulty
        WHERE {{
          ?con rdf:type codr:Contest ;
                ns:id           ?id  ;
                codr:name       ?name ;
                codr:desc       ?desc ;
                codr:start_date ?start_date ;
                codr:end_date   ?end_date ;
                codr:IRI        ?IRI ;
                codr:difficulty ?difficulty ;
                codr:hasCategory ?cat .
          ?cat rdf:type codr:Category ;
             codr:domain      "{0}" .
        }}
        """.format(domain)
    if nr is not None:
        q += """LIMIT {0}""".format(str(nr))
    sparql.setQuery(q)
    results = sparql.query().convert()
    return serialize(results, False)

# print getChallenges("Scripting", 3)


def getChallenge(contest_id):
    sparql = get_sparql()
    sparql.setQuery(prefix + """
        SELECT ?name ?desc ?start_date ?end_date ?IRI ?difficulty
        WHERE {{
          [] rdf:type codr:Contest ;
                ns:id   "{0}" ;
                codr:name       ?name ;
                codr:desc       ?desc ;
                codr:start_date ?start_date ;
                codr:end_date   ?end_date ;
                codr:IRI        ?IRI ;
                codr:difficulty ?difficulty .
        }}
        """.format(contest_id))
    results = sparql.query().convert()
    return serialize(results)


def getSolutions(contest_id):
    sparql = get_sparql()
    sparql.setQuery(prefix + """
        SELECT ?id ?text ?lang ?repo ?submission_date
        WHERE {{
          ?s rdf:type         codr:Solution ;
             ns:id             ?id ;
             codr:text ?text ;
             doap:programming-language  ?lang ;
             doap:code-repository  ?repo ;
             codr:submission_date ?submission_date ;
             codr:proposedTo  ?con .
          ?con rdf:type       codr:Contest ;
             ns:id            "{0}" .
        }}
        """.format(contest_id))
    results = sparql.query().convert()
    return serialize(results)


def getSolution(solution_id):
    sparql = get_sparql()
    sparql.setQuery(prefix + """
        SELECT ?text ?lang ?repo ?submission_date
        WHERE {{
          [] rdf:type codr:Solution ;
                ns:id   "{0}" ;
                codr:text ?text ;
                doap:programming-language  ?lang ;
                doap:code-repository  ?repo ;
                codr:submission_date ?submission_date .
        }}
        """.format(solution_id))
    results = sparql.query().convert()
    results = serialize(results, False)
    # Extract solution from mongoDB
    text = solutions.find_one({"id": solution_id})
    if text is not None:
        results[0]["text"] = text["text"]
    return json.dumps(results, indent=1)


def addSolutionToContest(solution_id, contest_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
            ?sol codr:proposedTo ?con
        }}
        WHERE {{
          ?sol rdf:type codr:Solution ;
                ns:id   "{0}" .
          ?con rdf:type codr:Contest ;
                ns:id   "{1}" .
        }}
        """.format(solution_id, contest_id))
    results = sparql.query()


def addSolution(solution_id, text, lang, repository, submission_date, contest_id):
    sparql = get_sparql(True)
    # date format 2017-01-30
    sparql.setQuery(prefix + """
        INSERT {{
          [] rdf:type                   codr:Solution ;
             ns:id                      "{0}" ;
             codr:text                  "{1}" ;
             doap:programming-language  "{2}" ;
             doap:code-repository       "{3}" ;
             codr:submission_date       "{4}"^^xsd:date .
        }} WHERE {{
               FILTER NOT EXISTS {{
                  [] rdf:type codr:Solution ;
                     ns:id    "{0}" .
          }}
        }}
        """.format(solution_id, text, lang, repository, submission_date))
    results = sparql.query()
    addSolutionToContest(solution_id, contest_id)


def addTagToContest(contest_id, tag_name):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
          ?c codr:hasTag ?tag .
        }}
        WHERE {{
          ?c  rdf:type codr:Contest ;
              ns:id "{0}" .
          ?tag rdf:type codr:Tag ;
               codr:name  "{1}" .
        }}
    """.format(contest_id, tag_name))
    results = sparql.query()


def addCategoryToContest(contest_id, categ_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
          ?c codr:hasCategory ?cat .
        }}
        WHERE {{
          ?c  rdf:type codr:Contest ;
              ns:id "{0}" .
          ?cat rdf:type codr:Category ;
              ns:id "{1}" .
        }}
    """.format(contest_id, categ_id))
    results = sparql.query()


def addChallenge(contest_id, name, desc, url, start_date, end_date, difficulty, categ_id, domain, tags):
    sparql = get_sparql(True)
    # date format 2017-01-30
    sparql.setQuery(prefix + """
        INSERT {{
          [] rdf:type         codr:Contest ;
             ns:id            "{0}" ;
             codr:name        "{1}" ;
             codr:desc        "{2}" ;
             codr:IRI         "{3}" ;
             codr:start_date "{4}"^^xsd:date ;
             codr:end_date "{5}"^^xsd:date ;
             codr:difficulty "{6}" ;
             codr:domain     "{7}" .
        }}
        WHERE {{
          FILTER NOT EXISTS {{
            [] rdf:type   codr:Contest;
               ns:id      "{0}"
          }}
        }}
        """.format(contest_id, name, desc, url, start_date, end_date, difficulty, domain))
    results = sparql.query()
    addCategoryToContest(contest_id, categ_id)
    tags = tags.split(",")
    for tag in tags:
        addTag(tag, "Tag description")
        addTagToContest(contest_id, tag)


def setParticipants(contest_id, nr):
    sparql = get_sparql(True)
    # date format 2017-01-30
    sparql.setQuery(prefix + """
            INSERT {{
              ?c codr:participants {1} .
            }}
            WHERE {{
                ?c rdf:type   codr:Contest ;
                   ns:id      "{0}" .
            }}
            """.format(contest_id, nr))
    results = sparql.query()


def removeChallenge(contest_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        DELETE {{
          ?c ?p ?s
        }} WHERE {{
          ?c rdf:type codr:Contest ;
             ns:id "{0}" ;
             ?p ?s .
        }}
    """.format(contest_id))
    results = sparql.query()


def removeSolution(solution_id):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        DELETE {{
          ?c ?p ?s
        }} WHERE {{
          ?c rdf:type codr:Solution ;
             ns:id "{0}" ;
             ?p ?s .
        }}
    """.format(solution_id))
    results = sparql.query()


def addTag(tag_name, tag_desc):
    sparql = get_sparql(True)
    sparql.setQuery(prefix + """
        INSERT {{
            [] rdf:type codr:Tag ;
            codr:name 	"{0}" ;
            codr:description "{1}" .
        }}
        WHERE {{
            FILTER NOT EXISTS {{
                [] rdf:type codr:Tag ;
                    codr:name "{0}" .
            }}
        }}
    """.format(tag_name, tag_desc))
    results = sparql.query()

def filterChallenges(participants, difficulty, IRI, tags):
    sparql = get_sparql()
    q = prefix + """
            SELECT ?name ?desc ?IRI ?difficulty ?participants
            WHERE {{
                ?c rdf:type codr:Contest ;
                    codr:name       ?name ;
                    codr:desc       ?desc ;
                    codr:start_date ?start_date ;
                    codr:end_date   ?end_date ;
                    codr:IRI        ?IRI ;
                    codr:difficulty ?difficulty ;
                    codr:participants 	?participants ; """
    if difficulty is not None:
        q += """
                    codr:difficulty "{0}" ;""".format(difficulty)
    if IRI is not None:
        q += """
                    codr:IRI        "{0}" ;""".format(IRI)
    if tags is not None and tags != "":
        for tag in tags.split(","):
            q += """
                        codr:hasTag     "{0}" ;""".format(tag)
    q += """
                    ns:id   ?id ."""
    if participants is not None:
        q += """
                    FILTER (?participants > {0})""".format(participants)
    q += """
            }}"""
    print q
    sparql.setQuery(q)
    results = sparql.query().convert()
    return serialize(results)


def do_sparql_query(q, JSON_LD=False):
    sparql = get_sparql()
    sparql.setQuery(q)
    results = sparql.query().convert()
    if JSON_LD:
        sparql.setReturnFormat(JSON_LD)
    else:
        sparql.setReturnFormat(RDFXML)
    return results


# print filterChallenges(None, None, None, "html")

tags = {
    "languages": "dbc:Algorithm_description_languages",
    "strings": "dbc:Algorithms_on_strings",
    "checksum": "dbc:Checksum_algorithms",
    "statistics": "dbc:Computational_statistics",
    "algebra": "dbc:Computer_algebra",
    "arithmetic": "dbc:Computer_arithmetic_algorithms",
    "graphs": "dbc:Graph_algorithms",
    "numerics": "dbc:Numerical_analysis",
    "optimizations": "dbc:Optimization_algorithms_and_methods",
    "recursion": "dbc:Recursion",
    "schedules": "dbc:Scheduling_algorithms",
    "dfs and similar": "dbc:Search_algorithms",
    "sortings": "dbc:Sorting_algorithms",
    "concurency": "dbc:Concurrent_algorithms",
    "distributions": "dbc:Distributed_algorithms",
    "networking": "dbc:Networking_algorithms",
    "heuristics": "dbc:Heuristic_algorithms",
    "databases": "dbc:Database_algorithms",
    "combinatorics": "dbc:Combinatorial_algorithms",
    "number_theory": "dbc:Computational_number_theory",
    "randomizations": "dbc:Randomized_algorithms",
    "ml": "dbc:Machine_learning_algorithms",
    "sls": "dbc:Stochastic_algorithms"
}


def queryDBPediaAlgorithms(type):
    if type in tags:
        type = tags[type]
    else:
        return []
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")

    sparql.setQuery("""
        SELECT ?label (SAMPLE(?abs) AS ?abs) (SAMPLE(?url) AS ?url) WHERE {{
            [] rdf:type yago:WikicatAlgorithms ;
                dbo:abstract ?abs ;
                dct:subject {0} ;
                dbo:wikiPageExternalLink ?url ;
                rdfs:label ?label .
            FILTER (lang(?label) = 'en')
        }} GROUP BY ?label LIMIT 5
    """.format(type))
    sparql.method = 'POST'
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return serialize(results, False)


def getRecommendations(tags):
    rec = []
    for tag in tags.split(","):
        results = queryDBPediaAlgorithms(tag)
        print results
        for res in results:
            rec.append(res)
    return rec


codeforces_tags = ["graphs","strings", "dfs and similar", "sortings", "schedules", "number theory"]

# print queryDBPediaAlgorithms(tags["arithmetic"])