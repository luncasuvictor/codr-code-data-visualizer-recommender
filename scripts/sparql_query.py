import json
from SPARQLWrapper import SPARQLWrapper, JSON

stardog_update = "http://localhost:5820/CoDR/update"
stardog_query = "http://localhost:5820/CoDR/query"

def upload_PERSON(user_id, first_name, last_name):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX doap: <http://usefulinc.com/ns/doap#>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
              [] rdf:type       foaf:Person ;
                 ns:id          "{0}" ;
                 foaf:first_name "{1}" ;
                 foaf:last_name "{2}" ;
                 foaf:mbox      "{0}@codecamp.com" .
            }} WHERE {{
               FILTER NOT EXISTS {{
                  [] rdf:type    foaf:Person ;
                     ns:id      "{0}" .
              }}
            }}""".format(user_id, first_name, last_name))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_PERSON("GURU")

def upload_CATEGORY(categ_id, cat_name):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX doap: <http://usefulinc.com/ns/doap#>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
                [] rdf:type codr:Category ;
                ns:id		"{0}" ;
                codr:name 	"{1}" .
            }}
            WHERE {{
                 FILTER NOT EXISTS {{
                  [] rdf:type    codr:Category ;
                     ns:id      "{0}" .
              }}
            }}""".format(categ_id, cat_name))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_CATEGORY("CATEGORY")

def upload_SOLUTION(sol_id, text, codeRep, lang):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX doap: <http://usefulinc.com/ns/doap#>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
                [] rdf:type                 codr:Solution ;
                ns:id		                "{0}" ;
                codr:text 	                "{1}" ;
                doap:code-repository:       "{2}" ;
                doap:programming-language:  "{3}" .
            }}
            WHERE {{
                 FILTER NOT EXISTS {{
                  [] rdf:type    codr:Solution ;
                     ns:id      "{0}" .
              }}
            }}""".format(sol_id, text, codeRep, lang))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_SOLUTION("SOLUTION", "http:// \\n\\\" ", "http://1", "c++")

def upload_CONTEST(con_id, con_name):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
                [] rdf:type codr:Contest ;
                ns:id		"{0}" ;
                codr:name 	"{1}"
            }}
            WHERE {{
                 FILTER NOT EXISTS {{
                  [] rdf:type    codr:Contest ;
                     ns:id      "{0}" .
              }}
            }}""".format(con_id, con_name))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_CONTEST("CONTEST")

def upload_SET_CONTEST(con_id, categ_id):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
                ?con codr:hasCategory ?cat .
            }}
            WHERE {{
                ?con rdf:type codr:Contest ;
                     ns:id "{0}" .
                ?cat rdf:type codr:Category ;
                     ns:id "{1}" .
            }}""".format(con_id, categ_id))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_SET_CONTEST("CONTEST", "CATEGORY")

def upload_SET_SOLUTION(user_id, sol_id):
    sparql = SPARQLWrapper(stardog_update)
    sparql.setQuery("""
            PREFIX ns: <http://rdfs.org/sioc/ns#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX codr: <http://students.info.uaic.ro/~victor.luncasu/ontologies/CoDR.owl#>

            INSERT {{
                ?per codr:implements ?sol .
                ?sol codr:implementedBy ?per .
            }}
            WHERE {{
                ?per rdf:type foaf:Person ;
                    ns:id "{0}" .
                ?sol rdf:type codr:Solution ;
                     ns:id "{1}" .
            }}""".format(user_id, sol_id))
    sparql.setCredentials("admin","admin")
    sparql.method = 'POST'
    results = sparql.query()

# upload_SET_SOLUTION("GURU", "SOLUTION", "CONTEST")