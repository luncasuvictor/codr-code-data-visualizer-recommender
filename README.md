# CoDR (Code Data Visualizer & Recommender) #

Using the public data offered by the 
[Free Code Camp](http://academictorrents.com/details/030b10dad0846b5aecc3905692890fb02404adbf), 
implement a Web service-oriented application able to explore, 
filter, visualize, and update the information regarding the participants to various programming competitions and their 
submitted solutions. The system will be able to provide useful recommendations about additional algorithm-related problems, 
contests, challenges, tutorials, public code repositories. In conjunction to 
[DBpedia](http://wiki.dbpedia.org/OnlineAccess) and/or [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page), 
the access to desired data and functionalities will be given via a SPARQL endpoint in various formats 
(at least HTML + RDFa and JSON-LD having embedded [software source code](https://schema.org/SoftwareSourceCode) 
schema.org constructs). 
See also, [Stack Exchange Data Explorer](https://data.stackexchange.com/help).

Project Blog: [https://codrapp.wordpress.com](https://codrapp.wordpress.com/)

Requirements Analysis: [Requirements Analysis.docx](https://goo.gl/sc36dT)

Technical Report: [Technical Report(drive version)](https://goo.gl/TY4bZh)

Application Link: [Application Link](http://codrapp.tk)